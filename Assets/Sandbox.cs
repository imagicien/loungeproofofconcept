﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class ObjectGroup {
	private GameObject gameObject;
	private ArrayList objects = new ArrayList();

	private Color _color;

	private uint _minBin, _maxBin;

	private float curValue = 0;
	private float maxValue = 0;

	private Dictionary<string, float> _data;

	//TODO: 	Utiliser maximum local?? Sinon 100% intensité rarement atteint! --> Tentative avec diminution progressive de maxValue
	//			Rotation du groupe selon pattern random
	//			Normaliser nommage

	public ObjectGroup(Dictionary<string, float> data, GameObject type, int radius, int number, uint minBin, uint maxBin, Color color) {
		_data = data;
		_minBin = minBin;
		_maxBin = maxBin;
		_color = color;

		var emptyGameObject = new GameObject ();
		gameObject = (GameObject)GameObject.Instantiate (emptyGameObject, Vector3.zero, Quaternion.identity);
		gameObject.name = "objectGroup";
		Object.Destroy (emptyGameObject);

		float arc = Mathf.PI * 2.0f / number;
		Vector3 initPos;
		for (int i = 0; i < number; i++) {
			initPos = new Vector3 (
				radius * Mathf.Cos(arc * i),
				data["yPos"],
				radius * Mathf.Sin(arc * i)
			);
			var new_obj = (GameObject)GameObject.Instantiate( type, initPos, Quaternion.identity );
			new_obj.transform.parent = gameObject.transform;
			objects.Add (new_obj);
		}
	}

	public void Update(float[] spectrum, float multiplier, float downSpeed, float rotSpeed, int minBin = -1, int maxBin = -1) {
		if (minBin != -1)
			_minBin = (uint)minBin;
		if (maxBin != -1)
			_maxBin = (uint)maxBin;

		if (_maxBin < _minBin)
			throw new UnityException ("maxBin < minBin... get your shit together");

		// Visualisation
		float value = 0.0f;
		for (uint b = _minBin; b <= _maxBin; b++) {
			value += spectrum [b] * spectrum [b];
		}
		if (value > maxValue)
			maxValue = value;

		if (value > curValue)
			curValue = value;

		foreach (GameObject obj in objects) {
			var p = new Vector3 (
				obj.transform.localPosition.x,
				_data["yPos"] + curValue * multiplier,
				obj.transform.localPosition.z
			);
			obj.transform.localPosition = p;

			var loweredValue = curValue / maxValue * 0.9f;
			var color = _color * new Color (loweredValue, loweredValue, loweredValue) + new Color (0.1f, 0.1f, 0.1f);
			obj.GetComponent<Renderer> ().material.SetColor("_EmissionColor", color);
		}
			
		curValue -= downSpeed * Time.deltaTime;
		if (curValue < 0.0f)
			curValue = 0.0f;

		maxValue -= 0.004f * Time.deltaTime;

		// Rotating cool stuff
		gameObject.transform.Rotate(new Vector3(0, 360 * rotSpeed * Time.deltaTime, 0));
	}

	public void Reset(){
		maxValue = 0;
	}
}

public class Sandbox : MonoBehaviour {

	public AudioSource audioSource;

	public GameObject objectType1;
	public GameObject objectType2;

	[Range(0.0f, 20.0f)]
	public float multiplier;

	[Range(0.0f, 0.2f)]
	public float downSpeed;

	[Range(5, 254)]
	public int trebMinBin;

	[Range(5, 254)]
	public int trebMaxBin;

	private Dictionary<string, float> properties{
		get{ return containers [selectedContainer]; }
	}
	private Dictionary<string, float> propertiesBlueprint = new Dictionary<string, float>(){
		{ "yPos", 0 },
		{ "Bananes", 12 },
		{ "Tomates", 6 },
		{ "Patates", 2 }
	};
	private List<Dictionary<string, float>> containers = new List<Dictionary<string, float>>();
	private string selectedProperty;
	private int selectedContainer;

	private ObjectGroup objGroup;
	private ObjectGroup objGroup2;

	private float[] spectrum = new float[256];

	// Use this for initialization
	void Start () {
		multiplier = 20.0f;
		downSpeed = 0.1f;
		trebMinBin = 17;
		trebMaxBin = 255;

		audioSource = GetComponent<AudioSource> ();

		selectedContainer = 0;
		if (false) {
			containers.Add(new Dictionary<string, float> (propertiesBlueprint));
			containers.Add(new Dictionary<string, float> (propertiesBlueprint));
		} else {
			LoadFile();
		}

		objGroup = new ObjectGroup (containers[0], objectType1, 10, 12, 0, 4, Color.red);
		objGroup2 = new ObjectGroup (containers[1], objectType2, 8, 18, 5, 255, Color.green);

		selectedProperty = properties.First ().Key;
	}

	// Update is called once per frame
	void Update () {
		audioSource.GetSpectrumData (spectrum, 0, FFTWindow.BlackmanHarris);
		objGroup.Update (spectrum, multiplier * -1f, downSpeed, 0.02f);
		objGroup2.Update (spectrum, multiplier * 8f, downSpeed * 0.5f, -0.04f, trebMinBin, trebMaxBin);
	}

	void OnGUI() {
		if (Event.current.Equals (Event.KeyboardEvent ("r"))) {
			objGroup.Reset ();
			objGroup2.Reset ();
		}

		if (Event.current.Equals (Event.KeyboardEvent ("left"))) {
			selectedContainer--;
			if (selectedContainer < 0) {
				selectedContainer = containers.Count - 1;
			}
		}
		if (Event.current.Equals (Event.KeyboardEvent ("right"))) {
			selectedContainer++;
			if (selectedContainer >= containers.Count) {
				selectedContainer = 0;
			}
		}

		if (Event.current.Equals (Event.KeyboardEvent ("up"))) {
			try{
				selectedProperty = properties.Keys.Reverse().SkipWhile (k => k != selectedProperty).Skip (1).First ();
			}
			catch(System.InvalidOperationException){
				// à une extrémité des choix possibles
			}
		}
		if (Event.current.Equals (Event.KeyboardEvent ("down"))) {
			try{
				selectedProperty = properties.Keys.SkipWhile (k => k != selectedProperty).Skip (1).First ();
			}
			catch(System.InvalidOperationException){
				// à une extrémité des choix possibles
			}
		}

		if (Event.current.type == EventType.ScrollWheel) {
			properties [selectedProperty] += (int)Event.current.delta.y;
		}

		// Draw GUI
		GUI.Label(new Rect(10, 10, 200, 20), "ObjectGroup " + selectedContainer);

		int i = 0;
		foreach(var p in properties){
			GUI.Label(new Rect(10, 40 + 20 * i, 200, 20), p.Key + " = " + p.Value.ToString() + (p.Key == selectedProperty ? "  <<<" : ""));
			i++;
		}
	}

	void OnApplicationQuit() {
		//Sauvegarder le data
		using (var fichier = new FileStream ("lounge.config", FileMode.Create)) {

			BinaryWriter writer = new BinaryWriter (fichier);
			writer.Write (containers.Count);

			foreach (var c in containers) {
				Serialize (c, fichier);
			}
		}
	}

	// Save / Load Data
	void LoadFile() {
		using (var fichier = new FileStream ("lounge.config", FileMode.Open)) {
			BinaryReader reader = new BinaryReader(fichier);
			int count = reader.ReadInt32();
			for (int n = 0; n < count; n++) {
				containers.Add ( Deserialize(fichier) );
			}
		}
	}

	public void Serialize(Dictionary<string, float> dictionary, Stream stream)
	{
		BinaryWriter writer = new BinaryWriter(stream);
		writer.Write(dictionary.Count);
		foreach (var kvp in dictionary)
		{
			writer.Write(kvp.Key);
			writer.Write((double)kvp.Value);
		}
		writer.Flush();
	}

	public Dictionary<string, float> Deserialize(Stream stream)
	{
		BinaryReader reader = new BinaryReader(stream);
		int count = reader.ReadInt32();
		var dictionary = new Dictionary<string,float>(count);
		for (int n = 0; n < count; n++)
		{
			var key = reader.ReadString();
			var value = (float)reader.ReadDouble ();
			dictionary.Add(key, value);
		}
		return dictionary;                
	}
}